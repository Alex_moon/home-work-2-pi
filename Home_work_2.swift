func getPi (numberOfIteration: Int = 100 ) -> (Double){
var i = 1.0
var count = 0.0
var result = 0.0
var check = true
switch numberOfIteration {
    case 1...Int.max:
        while count < Double(numberOfIteration) {
            let element = 4.0/i
                if check == true {
                result = result + element
                check = false
                } else {
                    result = result - element
                    check = true    
                }
            print ("Iteration \(Int(count+1))    = \(result)")
            i += 2
            count += 1
        }
    default : return 0
    }
    return result
}

func getSignAfterCommaInPi (sign: Int = 3 ) -> (Double) {
var i = 1.0
var count = 1
var result1 = 0.0
var result2 = 0.0
var check = true
var total = ""
switch sign {
    case 1...14:
        while count <= sign {
            let element1 = 4.0/i
            let element2 = 4.0/(i+2)
            if check {
                result1 = result1 + element1
                result2 = result1 - element2
                check = false
            } else {
                    result1 = result1 - element1
                    result2 = result1 + element2
                    check = true    
            }
            print("Iteration 1  = \(result1)")
            print("Iteration 2  = \(result2)")
        var result1String = String(result1)
        let result2String = String(result2)
        let index1 = result1String.index(result1String.startIndex, offsetBy: count+1)
        let index2 = result2String.index(result2String.startIndex, offsetBy: count+1)
            if result1String[index1] == result2String[index2] {
                let range = result1String.index(result1String.startIndex, offsetBy: count+2)..<result1String.endIndex
                result1String.removeSubrange(range)
                total = result1String
                count += 1
            }
        i += 2
        }
    default: total = "0"
    }
    return Double(total)!
}

print ("     Pi = \(getPi(numberOfIteration:1000))")
print ("Pi was truncated \(getSignAfterCommaInPi(sign: 3))")
